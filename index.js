"use strict"

const p$request = require("@ptyps/request")
const p$ws = require("@ptyps/ws")
const events = require("events")

module.exports.Gateway = function Gateway() {
  if (!(this instanceof Gateway))
    return new Gateway()

  this.__proto__ = Object.assign({}, events.EventEmitter.prototype)

  let socket = new p$ws.Socket("wss://gateway.discord.gg/?v=6&encoding=json")

  socket.on('data', buffer => {
    let json = JSON.parse(buffer)

    this.emit("packet", {
      ... !json.op ? null : {opcode: json.op},
      ... !json.d ? null : {data: json.d},
      ... !json.s ? null : {sequence: json.s},
      ... !json.t ? null : {event: json.t},
    })
  })

  for (let event of ["open", "close"])
    socket.on(event, () => this.emit(event))

  Object.assign(this, {
    connect: () => socket.connect(),

    out: (json = {}) => {
      let buffer = JSON.stringify({
        ... !json.opcode ? null : {op: json.opcode},
        ... !json.data ? null : {d: json.data},
        ... !json.sequence ? null : {s: json.sequence},
        ... !json.event ? null : {t: json.event},
      })

      return socket.out(buffer)
    }
  })
}